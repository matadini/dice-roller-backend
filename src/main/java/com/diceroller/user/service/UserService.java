package com.diceroller.user.service;

import com.diceroller.user.model.domain.User;
import com.diceroller.user.model.domain.UserCreator;
import com.diceroller.user.model.domain.CustomUserDetails;
import com.diceroller.user.model.domain.UserType;
import com.diceroller.user.model.entity.UserEntity;
import com.diceroller.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class UserService {
    @Autowired
    private UserRepository userRepository;
    private UserCreator userCreator = new UserCreator();


    public void clearRoom(final String roomId) {
        this.userRepository.deleteByRoomId(roomId);
    }


    public User createPlayer(final CustomUserDetails userDetails) {
        return userCreator.from(userDetails);
    }

    public User generateGameMaster(final String roomId, final String nickname) {
        return this.generateUser(roomId, nickname, UserType.GAME_MASTER);
    }

    public User generatePlayer(final String roomId, final String nickname) {
        return this.generateUser(roomId, nickname, UserType.ORDINARY_PLAYER);
    }

    private User generateUser(final String roomId, final String nickname, final UserType userType) {
        final UserEntity userEntity = new UserEntity(
                UUID.randomUUID().toString(),
                "{noop}" + UUID.randomUUID().toString(),
                roomId,
                nickname,
                userType
        );
        log.info("save user: " + userEntity.toString());
        this.userRepository.save(userEntity);

        return this.userCreator.from(new CustomUserDetails(userEntity));
    }
}

