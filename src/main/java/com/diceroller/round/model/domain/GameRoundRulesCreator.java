package com.diceroller.round.model.domain;

import com.diceroller.user.model.domain.User;
import com.diceroller.user.model.domain.UserCreator;
import com.diceroller.user.model.domain.UserType;
import com.diceroller.round.model.dto.GameRoundRulesDTO;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class GameRoundRulesCreator {
    public GameRoundRules from(final GameRoundRulesDTO dto, final String roomId) {
        requireNonNull(dto);

        final UserCreator userCreator = new UserCreator();
        final List<User> players = dto.getPlayers()
                .stream()
                .map(player -> userCreator.from(player, roomId, UserType.ORDINARY_PLAYER))
                .collect(Collectors.toList());

        return GameRoundRules.builder()
                .dices(dto.getDices())
                .players(players)
                .build();
    }
}
