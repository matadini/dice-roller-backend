package com.diceroller.round.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class DiceRollSetupDTO {
    @Getter
    private List<Long> dices;
}
