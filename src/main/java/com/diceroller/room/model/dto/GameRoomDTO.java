package com.diceroller.room.model.dto;

import com.diceroller.user.model.dto.UserDTO;
import com.diceroller.round.model.dto.ActualGameRoundDTO;
import com.diceroller.round.model.dto.RoundPlayerResultDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class GameRoomDTO {
    @Getter
    private String roomId;
    @Getter
    private List<Long> dices;
    @Getter
    private List<UserDTO> players;
    @Getter
    private boolean open;
    @Getter
    private List<RoundPlayerResultDTO> lastRound;
    @Getter
    private ActualGameRoundDTO actualRound;
    @Getter
    private UserDTO gameMaster;
}
