package com.diceroller.room;

import com.diceroller.room.model.dto.GameRoomCredentialsDTO;
import com.diceroller.room.model.dto.GameRoomDTO;
import com.diceroller.room.model.dto.GameRoomJoinDTO;
import com.diceroller.room.model.dto.GameRoomSetupDTO;
import com.diceroller.room.service.GameRoomCredentialsModelAssembler;
import com.diceroller.room.service.GameRoomModelAssembler;
import com.diceroller.room.service.GameRoomsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import javax.validation.Valid;

@RequestMapping("api/room")
@RestController
@Slf4j
public class GameRoomController {
    @Autowired
    private GameRoomsService gameRoomsService;
    @Autowired
    private GameRoomCredentialsModelAssembler gameRoomCredentialsModelAssembler;
    @Autowired
    private GameRoomModelAssembler gameRoomModelAssembler;


    @GetMapping(path = "/room-info-stream")
    public Flux<ServerSentEvent<GameRoomDTO>> getRoomInfoStream(@RequestParam String roomId) {
        return this.gameRoomsService.getGameRoomStream(roomId)
                .map(model -> model.asDTO())
                .map(room -> ServerSentEvent.<GameRoomDTO>builder()
                        .data(room)
                        .build());
    }

    @PostMapping("/create-room")
    public EntityModel<GameRoomCredentialsDTO> createGameRoom(@Valid @RequestBody GameRoomSetupDTO gameRoomSetupDTO) {
        final GameRoomCredentialsDTO gameRoom = this.gameRoomsService.createRoomAndGM(gameRoomSetupDTO);
        return this.gameRoomCredentialsModelAssembler.toGameMasterModel(gameRoom);
    }

    @PatchMapping("/destroy-room")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> destroyRoom(@RequestParam String roomId) {
        this.gameRoomsService.destroyGameRoom(roomId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/close-room")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> closeRoom(@RequestParam String roomId) {
        this.gameRoomsService.closeGameRoom(roomId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/open-room")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> openRoom(@RequestParam String roomId) {
        this.gameRoomsService.openGameRoom(roomId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PostMapping("/join-room")
    public EntityModel<GameRoomCredentialsDTO> joinRoom(@Valid @RequestBody GameRoomJoinDTO gameRoomJoinDTO) {
        final GameRoomCredentialsDTO gameRoom = this.gameRoomsService.joinRoom(gameRoomJoinDTO);
        return this.gameRoomCredentialsModelAssembler.toModel(gameRoom);
    }

}
