package com.diceroller.room.model.dto;

import com.diceroller.user.model.dto.UserCredentialsDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class GameRoomCredentialsDTO {
    @Getter
    private String roomId;
    @Getter
    private UserCredentialsDTO credentials;
}
