package com.diceroller.management;

import com.diceroller.management.model.SuperUserCredentialsDTO;
import com.diceroller.room.model.dto.GameRoomDTO;
import com.diceroller.room.service.GameRoomModelAssembler;
import com.diceroller.room.service.GameRoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("api/management")
@RestController
public class ManagementController {
    @Autowired
    private GameRoomsService gameRoomsService;
    @Autowired
    private GameRoomModelAssembler gameRoomModelAssembler;
    @Autowired
    private ManagementService managementService;


    @PostMapping("/authenticate")
    @PreAuthorize("hasAnyAuthority('SUPER_USER')")
    public ResponseEntity<?> authenticate(@Valid @RequestBody SuperUserCredentialsDTO credentials) {
        return ResponseEntity
                .ok(true);
    }

    @GetMapping(path = "/room/list")
    @PreAuthorize("hasAnyAuthority('SUPER_USER')")
    public CollectionModel<EntityModel<GameRoomDTO>> getRooms() {
        final List<GameRoomDTO> rooms = this.gameRoomsService.getRooms();
        return this.gameRoomModelAssembler.toCollectionModel(rooms);
    }

    @PatchMapping("/room/destroy")
    @PreAuthorize("hasAnyAuthority('SUPER_USER')")
    public ResponseEntity<?> destroyRooms(@RequestParam List<String> roomId) {
        managementService.destroyExistingRooms(roomId);
        return ResponseEntity
                .ok()
                .build();
    }
}
