package com.diceroller.round.model.dto;

import com.diceroller.user.model.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class GameRoundRulesDTO {
    @Getter
    private List<Long> dices;
    @Getter
    private List<UserDTO> players;
}
