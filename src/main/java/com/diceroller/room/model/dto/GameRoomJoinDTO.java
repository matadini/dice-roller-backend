package com.diceroller.room.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
public class GameRoomJoinDTO {
    @Getter
    @NotNull
    private String roomPassword;
    @Getter
    @NotNull
    private String playerNickname;
}
