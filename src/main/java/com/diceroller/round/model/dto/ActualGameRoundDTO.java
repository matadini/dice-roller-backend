package com.diceroller.round.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ActualGameRoundDTO {
    @Getter
    private GameRoundRulesDTO rules;
    @Getter
    private boolean active;
}
