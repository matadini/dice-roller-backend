package com.diceroller.round.model.domain;

import com.diceroller.round.model.dto.DiceRollsResultsDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
public class DiceRollResults {
    List<DiceRoll> results;

    public DiceRollsResultsDTO asDTO() {
        return new DiceRollsResultsDTO(
                results.stream()
                .map(result -> result.asDTO())
                .collect(Collectors.toList())
        );
    }
}
