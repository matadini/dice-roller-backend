package com.diceroller.room.model.domain;

import com.diceroller.user.model.domain.User;
import com.diceroller.round.model.domain.DiceRollResults;
import com.diceroller.round.model.domain.GameRoundRules;
import lombok.AllArgsConstructor;
import lombok.Getter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxProcessor;
import reactor.core.publisher.FluxSink;

@AllArgsConstructor
public class GameRoomRx {
    private FluxProcessor<GameRoom, GameRoom> processor;
    private FluxSink sink;
    @Getter
    private GameRoom lastValue;


    public void complete() {
        this.sink.complete();
    }

    public void update() {
        this.sink.next(this.lastValue);
    }

    public Flux<GameRoom> getStream() {
        return this.processor.map(room -> room);
    }

    public void closeRoom() {
        this.lastValue.closeRoom();
        this.update();
    }

    public void openRoom() {
        this.lastValue.openRoom();
        this.update();
    }

    public void openRound(final GameRoundRules rules) {
        this.lastValue.openRound(rules);
        this.update();
    }

    public void closeRound() {
        this.lastValue.closeRound();
        this.update();
    }

    public void updateLastRound(final User player, final DiceRollResults diceRollResults) {
        this.lastValue.updateLastRound(player, diceRollResults);
        this.update();
    }

    public void addPlayer(final User player) {
        this.lastValue.getPlayers()
                .add(player);
        this.update();
    }
}
