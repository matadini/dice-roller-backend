package com.diceroller.round.model.domain;

import com.diceroller.user.model.domain.User;
import com.diceroller.round.model.dto.DiceRollsResultsDTO;
import com.diceroller.round.model.dto.RoundPlayerResultDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
public class RoundPlayerResult {
    private User player;
    private List<DiceRollResults> rolls;

    public RoundPlayerResultDTO asDTO() {
        final List<DiceRollsResultsDTO> rollDTOS = this.rolls.stream()
                .map(roll -> roll.asDTO())
                .collect(Collectors.toList());

        return new RoundPlayerResultDTO(
                this.player.asDTO(),
                rollDTOS
        );
    }
}
