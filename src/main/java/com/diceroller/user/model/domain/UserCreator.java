package com.diceroller.user.model.domain;

import com.diceroller.user.model.dto.UserDTO;

import static java.util.Objects.requireNonNull;

public class UserCreator {
    public User from(final UserDTO dto, final String roomId, final UserType userType) {
        requireNonNull(dto);
        requireNonNull(roomId);


        return User.builder()
                .username(dto.getUsername())
                .nickname(dto.getNickname())
                .roomId(roomId)
                .userType(userType)
                .build();
    }

    public User from(final CustomUserDetails userDetails) {
        requireNonNull(userDetails);

        return User.builder()
                .username(userDetails.getUsername())
                .password(userDetails.getPassword().substring("{noop}".length()))
                .roomId(userDetails.getRoomId())
                .nickname(userDetails.getNickname())
                .userType(userDetails.getUserType())
                .build();
    }
}
