package com.diceroller.round.model.dto;

import com.diceroller.user.model.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class RoundPlayerResultDTO {
    @Getter
    private UserDTO player;
    @Getter
    private List<DiceRollsResultsDTO> rolls;
}
