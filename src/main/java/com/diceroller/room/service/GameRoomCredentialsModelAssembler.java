package com.diceroller.room.service;

import com.diceroller.room.GameRoomController;
import com.diceroller.room.model.dto.GameRoomCredentialsDTO;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class GameRoomCredentialsModelAssembler implements RepresentationModelAssembler<GameRoomCredentialsDTO, EntityModel<GameRoomCredentialsDTO>> {

    @Override
    public EntityModel<GameRoomCredentialsDTO> toModel(GameRoomCredentialsDTO entity) {
        return new EntityModel<>(entity,
                linkTo(methodOn(GameRoomController.class).getRoomInfoStream(entity.getRoomId())).withSelfRel()
        );
    }

    public EntityModel<GameRoomCredentialsDTO> toGameMasterModel(GameRoomCredentialsDTO entity) {
        return this.toModel(entity)
                .add(
                        linkTo(methodOn(GameRoomController.class).destroyRoom(entity.getRoomId())).withRel("destroy"),
                        linkTo(methodOn(GameRoomController.class).closeRoom(entity.getRoomId())).withRel("close"),
                        linkTo(methodOn(GameRoomController.class).openRoom(entity.getRoomId())).withRel("open")
                );
    }
}
