package com.diceroller.round.model.domain;

import com.diceroller.round.model.dto.DiceRollSetupDTO;

public class DiceRollSetupCreator {
    public DiceRollSetup from(final DiceRollSetupDTO dto) {
        return DiceRollSetup.builder()
                .dices(dto.getDices())
                .strategy(new SimpleDiceRollStrategy())
                .build();
    }
}
