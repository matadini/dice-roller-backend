package com.diceroller.room.model.domain;

import com.diceroller.user.model.domain.User;
import com.diceroller.round.model.domain.ActualGameRound;
import com.diceroller.round.model.domain.DiceRollResults;
import com.diceroller.round.model.domain.GameRoundRules;
import com.diceroller.round.model.domain.RoundPlayerResult;
import com.diceroller.user.model.dto.UserDTO;
import com.diceroller.room.model.dto.GameRoomCredentialsDTO;
import com.diceroller.room.model.dto.GameRoomDTO;
import com.diceroller.round.model.dto.RoundPlayerResultDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
@Data
public class GameRoom {
    private String roomId;
    private List<Long> dices;
    private String password;
    private List<User> players;
    private boolean open;
    private List<RoundPlayerResult> lastRound;
    private User gameMaster;
    private ActualGameRound actualGameRound;

    public void closeRoom() {
        this.setOpen(false);
    }

    public void openRoom() {
        this.setOpen(true);
    }

    public void openRound(final GameRoundRules rules) {
        this.actualGameRound.open(rules);

        if (this.lastRound == null) {
            this.lastRound = new ArrayList<>();
        } else {
            this.lastRound.clear();
        }
    }

    public void closeRound() {
        this.actualGameRound.close();
    }

    public boolean isRoundActive() {
        return this.actualGameRound != null && this.actualGameRound.isActive();
    }

    public void updateLastRound(final User player, final DiceRollResults diceRollResults) {
        RoundPlayerResult result = null;
        for (int i = 0; i != this.lastRound.size() && result == null; ++i) {
            if (this.lastRound.get(i).getPlayer().equals(player)) {
                result = this.lastRound.get(i);
            }
        }

        if (result != null) {
            result.getRolls()
                    .add(diceRollResults);
        } else {
            final List<DiceRollResults> list = new ArrayList<>();
            list.add(diceRollResults);
            this.lastRound.add(new RoundPlayerResult(player, list));
        }
    }

    public GameRoomDTO asDTO() {
        final List<UserDTO> playerDTOS = this.getPlayers()
                .stream()
                .map(player -> player.asDTO())
                .collect(Collectors.toList());

        final List<RoundPlayerResultDTO> lastRoundDTOS = this.lastRound == null ? null
                : this.lastRound.stream()
                .map(entry -> entry.asDTO())
                .collect(Collectors.toList());

        return new GameRoomDTO(
                this.getRoomId(),
                this.getDices(),
                playerDTOS,
                this.isOpen(),
                lastRoundDTOS,
                this.getActualGameRound().asDTO(),
                this.gameMaster.asDTO()
        );
    }

    public GameRoomCredentialsDTO asGameRoomCredentialsMasterDTO() {
        return new GameRoomCredentialsDTO(this.getRoomId(), this.getGameMaster().asUserCredentialsDTO());
    }
}
