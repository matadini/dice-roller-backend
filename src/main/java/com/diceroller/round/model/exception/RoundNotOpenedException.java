package com.diceroller.round.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "ROUND_NOT_OPENED")
public class RoundNotOpenedException extends RuntimeException {
}
