package com.diceroller.room.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class GameRoomSetupDTO {
    @Getter
    @NotNull
    private List<Long> dices;
    @Getter
    @NotNull
    private String roomPassword;
    @Getter
    @NotNull
    private String gameMasterNickname;
}
