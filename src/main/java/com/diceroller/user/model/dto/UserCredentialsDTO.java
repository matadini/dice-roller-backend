package com.diceroller.user.model.dto;

import com.diceroller.user.model.domain.UserType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class UserCredentialsDTO {
    @Getter
    private String username;
    @Getter
    private String password;
    @Getter
    private UserType userType;
}
