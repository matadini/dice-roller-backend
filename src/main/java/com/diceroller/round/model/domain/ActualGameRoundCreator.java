package com.diceroller.round.model.domain;

import com.diceroller.round.model.dto.ActualGameRoundDTO;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class ActualGameRoundCreator {
    public ActualGameRound from(final ActualGameRoundDTO dto, final String roomId) {
        requireNonNull(dto);

        final Optional<GameRoundRules> rules = Optional.ofNullable(new GameRoundRulesCreator().from(dto.getRules(), roomId));
        return ActualGameRound.builder()
                .active(dto.isActive())
                .rules(rules)
                .build();
    }
}
