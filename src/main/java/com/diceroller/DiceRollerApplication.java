package com.diceroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication()
public class DiceRollerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiceRollerApplication.class, args);
	}

}
