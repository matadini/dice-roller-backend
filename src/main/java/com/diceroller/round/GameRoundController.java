package com.diceroller.round;

import com.diceroller.user.model.domain.CustomUserDetails;
import com.diceroller.round.model.dto.DiceRollSetupDTO;
import com.diceroller.round.model.dto.DiceRollsResultsDTO;
import com.diceroller.round.model.dto.GameRoundRulesDTO;
import com.diceroller.room.service.GameRoomsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/round")
@RestController
@Slf4j
public class GameRoundController {
    @Autowired
    private GameRoomsService gameRoomsService;

    @PostMapping("/open-round")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> openRound(@RequestBody GameRoundRulesDTO rules, @RequestParam String roomId) {
        this.gameRoomsService.openRound(roomId, rules);
        return ResponseEntity
                .ok()
                .build();
    }

    @PostMapping("/close-round")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> closeRound(@RequestParam String roomId) {
        this.gameRoomsService.closeRound(roomId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PostMapping("/make-roll")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER', 'ORDINARY_PLAYER')")
    public EntityModel<DiceRollsResultsDTO> makeARoll(@RequestParam String roomId, @RequestBody DiceRollSetupDTO diceRollSetupDTO,  Authentication authentication) {
        final DiceRollsResultsDTO results = this.gameRoomsService.makeARoll(diceRollSetupDTO, roomId, (CustomUserDetails) authentication.getPrincipal());
        return new EntityModel<>(results);
    }
}
