package com.diceroller.room.model.domain;

import com.diceroller.round.model.domain.ActualGameRound;
import com.diceroller.room.model.dto.GameRoomSetupDTO;

import java.util.ArrayList;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

public class GameRoomCreator {
    public GameRoom from(final GameRoomSetupDTO dto) {
        requireNonNull(dto);

        return GameRoom.builder()
                .roomId(UUID.randomUUID().toString())
                .dices(dto.getDices())
                .password(dto.getRoomPassword())
                .players(new ArrayList<>())
                .open(true)
                .lastRound(null)
                .gameMaster(null)
                .actualGameRound(new ActualGameRound())
                .build();
    }

}
