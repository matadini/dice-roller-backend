package com.diceroller.round.model.domain;

import com.diceroller.user.model.domain.User;
import com.diceroller.user.model.dto.UserDTO;
import com.diceroller.round.model.dto.GameRoundRulesDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
@Builder
public class GameRoundRules {
    private List<Long> dices;
    private List<User> players;

    public GameRoundRulesDTO asDTO() {
        final List<UserDTO> playerDTOS = this.getPlayers()
                .stream()
                .map(player -> player.asDTO())
                .collect(Collectors.toList());

        return new GameRoundRulesDTO(
                this.getDices(),
                playerDTOS
        );
    }
}
