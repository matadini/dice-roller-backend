package com.diceroller.room.service;

import com.diceroller.room.model.domain.GameRoom;
import com.diceroller.room.model.domain.GameRoomCreator;
import com.diceroller.room.model.domain.GameRoomRx;
import com.diceroller.round.model.domain.DiceRollResults;
import com.diceroller.round.model.domain.DiceRollSetupCreator;
import com.diceroller.round.model.domain.GameRoundRulesCreator;
import com.diceroller.user.model.domain.CustomUserDetails;
import com.diceroller.user.model.domain.User;
import com.diceroller.room.model.dto.GameRoomCredentialsDTO;
import com.diceroller.room.model.dto.GameRoomDTO;
import com.diceroller.room.model.dto.GameRoomJoinDTO;
import com.diceroller.room.model.dto.GameRoomSetupDTO;
import com.diceroller.round.model.dto.DiceRollSetupDTO;
import com.diceroller.round.model.dto.DiceRollsResultsDTO;
import com.diceroller.round.model.dto.GameRoundRulesDTO;
import com.diceroller.room.model.exception.RoomLimitReachedException;
import com.diceroller.room.model.exception.RoomNoAccessException;
import com.diceroller.room.model.exception.RoomNotFoundException;
import com.diceroller.room.model.exception.RoomNotOpenException;
import com.diceroller.round.model.exception.RoundNotOpenedException;
import com.diceroller.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.DirectProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxProcessor;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GameRoomsService {
    @Value("${game.room.limit}")
    private Long roomLimit;

    @Autowired
    private UserService userService;

    private Map<String, GameRoomRx> rooms = new ConcurrentHashMap<>();
    private GameRoomCreator gameRoomCreator = new GameRoomCreator();

    public List<GameRoomDTO> getRooms() {
        return this.rooms.values().stream()
                .map(rx -> rx.getLastValue())
                .map(room -> room.asDTO())
                .collect(Collectors.toList());
    }

    public GameRoomCredentialsDTO createRoomAndGM(final GameRoomSetupDTO room) throws RoomLimitReachedException {
        if (this.rooms.size() == this.roomLimit) {
            throw new RoomLimitReachedException();
        } else {
            return this.createGameRoom(room)
                    .asGameRoomCredentialsMasterDTO();
        }
    }

    private GameRoom createGameRoom(final GameRoomSetupDTO roomSetup) {
        final FluxProcessor<GameRoom, GameRoom> processor = DirectProcessor.<GameRoom>create().serialize();
        final GameRoom room = gameRoomCreator.from(roomSetup);
        final User gameMaster = this.userService.generateGameMaster(room.getRoomId(), roomSetup.getGameMasterNickname());
        room.setGameMaster(gameMaster);

        final GameRoomRx flux = new GameRoomRx(processor, processor.sink(), room);
        this.rooms.put(room.getRoomId(), flux);
        flux.update();
        return room;
    }

    public void destroyGameRoom(final String roomId) throws RoomNotFoundException {
        this.getGameRoomRxById(roomId)
                .complete();

        this.rooms.remove(roomId);
        this.userService.clearRoom(roomId);
    }

    public Flux<GameRoom> getGameRoomStream(final String roomId) throws RoomNotFoundException {
        final GameRoomRx room = this.getGameRoomRxById(roomId);
        return Flux.merge(Flux.just(room.getLastValue()), room.getStream());
    }

    public void closeGameRoom(final String roomId) throws RoomNotFoundException {
        this.getGameRoomRxById(roomId)
                .closeRoom();
    }

    public void openGameRoom(final String roomId) throws RoomNotFoundException {
        this.getGameRoomRxById(roomId)
                .openRoom();
    }

    public void openRound(final String roomId, final GameRoundRulesDTO rulesDTO) throws RoomNotFoundException {
        this.getGameRoomRxById(roomId)
                .openRound(new GameRoundRulesCreator().from(rulesDTO, roomId));
    }

    public void closeRound(final String roomId) throws RoomNotFoundException {
        this.getGameRoomRxById(roomId)
                .closeRound();
    }

    public DiceRollsResultsDTO makeARoll(final DiceRollSetupDTO diceRollSetupDTO, final String roomId, final CustomUserDetails userDetails) throws RoomNoAccessException, RoomNotFoundException, RoundNotOpenedException {
        log.info("player details: " + userDetails.toString());
        final User player = this.userService.createPlayer(userDetails);
        final GameRoomRx gameRoomRx = this.getGameRoomRxById(roomId);
        validatePlayerRollRequest(roomId, player, gameRoomRx);

        final DiceRollSetupCreator diceRollSetupCreator = new DiceRollSetupCreator();
        final DiceRollResults diceRollResults = player.rollDices(diceRollSetupCreator.from(diceRollSetupDTO));

        gameRoomRx.updateLastRound(player, diceRollResults);
        return diceRollResults.asDTO();
    }

    private void validatePlayerRollRequest(final String roomId, final User player, final GameRoomRx gameRoomRx) {
        log.info("player room: " + player.getRoomId());
        log.info("roomId: " + roomId);
        if (!player.isInRoom(roomId)) {
            throw new RoomNoAccessException();
        }

        if (!gameRoomRx.getLastValue().isRoundActive()) {
            throw new RoundNotOpenedException();
        }
    }

    public GameRoomCredentialsDTO joinRoom(final GameRoomJoinDTO gameRoomJoinDTO) throws RoomNotFoundException, RoomNotOpenException {
        final GameRoomRx gameRoomRx = this.getGameRoomRxByPassword(gameRoomJoinDTO.getRoomPassword());
        if (!gameRoomRx.getLastValue().isOpen()) {
            throw new RoomNotOpenException();
        }
        final User player = this.userService.generatePlayer(gameRoomRx.getLastValue().getRoomId(), gameRoomJoinDTO.getPlayerNickname());

        gameRoomRx.addPlayer(player);

        return new GameRoomCredentialsDTO(gameRoomRx.getLastValue().getRoomId(), player.asUserCredentialsDTO());
    }

    private GameRoomRx getGameRoomRxByPassword(final String password) throws RoomNotFoundException {
        return this.rooms.values()
                .stream()
                .filter(room -> room.getLastValue().getPassword().equals(password))
                .findFirst()
                .orElseThrow(() -> new RoomNotFoundException());
    }

    private GameRoomRx getGameRoomRxById(final String roomId) throws RoomNotFoundException {
        if (!this.roomExist(roomId)) {
            throw new RoomNotFoundException();
        }
        return this.rooms.get(roomId);
    }

    public boolean roomExist(String roomId) {
        return this.rooms.containsKey(roomId);
    }
}
