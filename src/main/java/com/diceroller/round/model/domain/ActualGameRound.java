package com.diceroller.round.model.domain;

import com.diceroller.round.model.dto.ActualGameRoundDTO;
import lombok.*;

import java.util.Optional;

@AllArgsConstructor
@Builder
public class ActualGameRound {
    @Getter
    private Optional<GameRoundRules> rules;
    @Getter
    private boolean active;

    public ActualGameRound() {
        this.rules = Optional.empty();
    }

    public void open(final GameRoundRules rules) {
        this.rules = Optional.ofNullable(rules);
        this.active = true;
    }

    public void close() {
        this.rules = Optional.empty();
        this.active = false;
    }

    public ActualGameRoundDTO asDTO() {
        return new ActualGameRoundDTO(
                this.rules.map(GameRoundRules::asDTO)
                        .orElse(null),
                this.active
        );
    }
}
