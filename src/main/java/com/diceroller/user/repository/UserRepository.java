package com.diceroller.user.repository;

import com.diceroller.user.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Modifying
    @Transactional
    @Query("delete from UserEntity u where u.roomId in ?1")
    void deleteByRoomId(final String roomId);

    UserEntity findByUsername(final String username);
}
