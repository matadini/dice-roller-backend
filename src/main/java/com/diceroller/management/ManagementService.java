package com.diceroller.management;

import com.diceroller.room.service.GameRoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManagementService {
    @Autowired
    private GameRoomsService gameRoomsService;

    public void destroyExistingRooms(List<String> rooms) {
        rooms.stream()
                .filter(roomId -> gameRoomsService.roomExist(roomId))
                .forEach(roomId -> gameRoomsService.destroyGameRoom(roomId));
    }
}
